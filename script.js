/*
1)	Змінну в JavaScript можна оголосити за допомогою ключових слів let та const;
2)	promt  виводить сповіщення та чекає, поки користувач введе текст, а потім повертає введене значення або null, якщо введення було  відмінено (cancel)
confirm виводить сповіщення та чекає, доки користувач натисне Ок або Cancel та повертає true/false.
3)	Неявне перетворення, це коли значення можуть бути конвертовані між різними типами даних автоматично.
let a = 13;
let b = "22";
result = a + b
console.log(result)
*/


/*1 завдання*/
let admin, name;
name = "Elena";
admin = name;
console.log(admin);


/* 2*/
const days = Math.floor(Math.random() * 10) + 1;
console.log(days * 60)


/* 3 */
const number = +prompt('Напишіть будь-яке число', '');
console.log(number)


